# Usage

    java -jar gittag.jar -commit *commit* -version *X.Y.Y* -release *Gold.5.5.it4* -project *SAN* -repo *repoSlug* -login *username* -password *password*

Launch it into the repository


## From Jenkins promotion

    java -jar gittag.jar -commit %GIT_COMMIT% -version %productVersion% -release *Gold.5.5.it4* -project *SAN* -repo *repoSlug* -login *username* -password *password*