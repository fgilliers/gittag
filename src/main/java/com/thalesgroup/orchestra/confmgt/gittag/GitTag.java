package com.thalesgroup.orchestra.confmgt.gittag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.api.FetchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.TagCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * Automatically create a pull request and put a tag
 *
 * @author T0140792
 */
public class GitTag {
  final Logger _logger = LoggerFactory.getLogger(GitTag.class);

  public static void main(final String[] args) {
    final GitTag gitTag = new GitTag();

    gitTag.getLogger().info("Application starting...");

    /*
     * Parse options
     */
    final OptionParser parser = new OptionParser();
    parser.accepts("commit").withRequiredArg();
    parser.accepts("version").withRequiredArg();
    parser.accepts("release").withRequiredArg();
    parser.accepts("project").withRequiredArg();
    parser.accepts("repo").withRequiredArg();
    parser.accepts("login").withRequiredArg();
    parser.accepts("password").withRequiredArg();

    final OptionSet options = parser.parse(args);

    /*
     * Get values
     */
    // Git commit
    final String commitOption = options.valueOf("commit").toString();
    gitTag.getLogger().info("Git commit is '{}'.", commitOption);

    // Product version
    final String versionOption = options.valueOf("version").toString();
    gitTag.getLogger().info("Product version is '{}'.", versionOption);

    // Orchestra release
    final String releaseOption = options.valueOf("release").toString();
    gitTag.getLogger().info("Orchestra release is '{}'.", releaseOption);

    // Bitbucket project
    final String projectOption = options.valueOf("project").toString();
    gitTag.getLogger().info("Bitbucket project is '{}'.", projectOption);

    // bitbucket repository slug
    final String repoSlugOption = options.valueOf("repo").toString();
    gitTag.getLogger().info("Bitbucket repository slug is '{}'.", repoSlugOption);

    // login
    final String loginOption = options.valueOf("login").toString();

    // password
    final String passwordOption = options.valueOf("password").toString();

    // split release on token comma ','
    // Tag
    final List<String> tagNames = new ArrayList<>();
    for (final String release : releaseOption.split(",")) {
      tagNames.add(versionOption + "." + release);
    }

    // set credentials
    final UsernamePasswordCredentialsProvider credentials = new UsernamePasswordCredentialsProvider(loginOption,
        passwordOption);

    // Git repository
    final FileRepositoryBuilder builder = new FileRepositoryBuilder();
    try (final Repository repository = builder.readEnvironment().findGitDir().build()) {
      try (final Git git = new Git(repository)) {

        /*
         * git fetch origin master
         */
        final FetchCommand fetch = git.fetch();
        fetch.setCheckFetchedObjects(true);
        if (options.has("login")) {
          fetch.setCredentialsProvider(credentials);
        }
        final List<RefSpec> refSpecs = new ArrayList<>();
        refSpecs.add(new RefSpec("refs/head/master:refs/remotes/origin/master"));
        fetch.setRefSpecs(refSpecs);
        fetch.setCredentialsProvider(credentials);
        gitTag.getLogger().info("fetching master from origin...");
        try {
          fetch.call();
        } catch (final GitAPIException exception_p) {
          gitTag.getLogger().error("Cannot execute Git fetch.");
          gitTag.getLogger().error(exception_p.toString());
          System.exit(1);
        }

        /*
         * git tag -a -m "1.0.0.Gold.5.5.it1" FETCH_HEAD
         */
        // get FETCH_HEAD
        final ObjectId fetchHead = repository.resolve("FETCH_HEAD");
        final List<Ref> tags = new ArrayList<>();
        try (RevWalk walk = new RevWalk(repository)) {
          final RevCommit commit = walk.parseCommit(fetchHead);

          for (final String tagName : tagNames) {
            final TagCommand tagCommand = git.tag();
            tagCommand.setAnnotated(true);
            tagCommand.setMessage(tagName);
            tagCommand.setName(tagName);
            tagCommand.setObjectId(commit);
            gitTag.getLogger().info("Creating tag '{}' on FETCH_HEAD...");
            try {
              tags.add(tagCommand.call());
            } catch (final GitAPIException exception_p) {
              gitTag.getLogger().error("Cannot execute Git tag {}.", tagNames);
              gitTag.getLogger().error(exception_p.toString());
              System.exit(1);
            }
          }
          walk.dispose();
        }

        // git push origin 1.0.0.Gold.5.5.it1
        final PushCommand push = git.push();
        for (final Ref tag : tags) {
          push.add(tag);
        }

        if (options.has("login")) {
          push.setCredentialsProvider(credentials);
        }
        gitTag.getLogger().info("Pushing tag '{}' to origin...", tagNames);
        try {
          push.call();
        } catch (final GitAPIException exception_p) {
          gitTag.getLogger().error("Cannot execute Git push.");
          gitTag.getLogger().error(exception_p.toString());
          System.exit(1);
        }
      }
    } catch (final IOException exception_p) {
      gitTag.getLogger().error("Cannot build Git repository.");
      gitTag.getLogger().error(exception_p.getMessage());
      System.exit(1);
    }

    gitTag.getLogger().info("Application exiting...");
    System.exit(0);
  }

  public Logger getLogger() {
    return _logger;
  }
}
